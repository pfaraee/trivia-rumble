Router.route('/', function () {
	this.layout('ApplicationLayout');
	this.render('home');
});

Router.route('/game/:_id', function () {
	this.layout('ApplicationLayout');
	this.render('game', {
		data: function () {
			// selects our game
			var gameId = this.params._id;
			var game = Games.findOne(gameId);
			
			// sets player indexes
			var opponentIndex,
				currentUserIndex;

			if (game.players[0].player === Meteor.userId()) {
				currentUserIndex = 0;
				opponentIndex = 1;
			} else if (game.players[1].player === Meteor.userId()) {
				currentUserIndex = 1;
				opponentIndex = 0;
			}

			Session.set('opponentIndex', opponentIndex);
			Session.set('currentUserIndex', currentUserIndex);

			return game;
		}
	});
}, {
	name: 'game'
});

Router.route('/search', function () {
	this.layout('ApplicationLayout');
	this.render('search');
});

Router.route('/submit', function () {
	this.layout('ApplicationLayout');
	this.render('submit');
});

Router.route('/profile/:_id', function () {
	this.layout('ApplicationLayout');
	this.render('profile', {
		data: function () {
			var user = this.params._id;

			return Meteor.users.findOne(user);
		}
	});
});