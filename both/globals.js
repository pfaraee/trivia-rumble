Games = new Mongo.Collection('games');
Trivia = new Mongo.Collection('trivia');

UsersIndex = new EasySearch.Index({
    collection: Meteor.users,
    fields: ['username'],
    engine: new EasySearch.Minimongo()
});

ProfilePictures = new FS.Collection("images", {
  stores: [new FS.Store.GridFS("images")]
});