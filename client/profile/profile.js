Template.profile.helpers({
	yourProfile: function () {
		if (this._id === Meteor.userId()){
			return true;
		} else {
			return false;
		}
	},
	profilePic: function (user) {
		var profilePictures;
		var pictureId;
		
		pictureId = user.profile.picture;
		profilePictures = ProfilePictures.find(pictureId).fetch();
		
    	return profilePictures[0]
  	}
});

Template.profile.events({
	'change .profile-upload': function(event, template) {
		FS.Utility.eachFile(event, function(file) {
      		ProfilePictures.insert(file, function (err, fileObj) {
        		Meteor.users.update(Meteor.userId(),{
					$set: {
						"profile.picture": fileObj._id
					}
				});
      		});
		});
	}
});