function Trivia(args) {
	args = args || {};
	this.question = args.question || "";
	this.choices = args.choices || "";
	this.correctAnswer = args.correctAnswer;
}

Template.submit.events({
	"submit .new-trivia": function (event) {
		event.preventDefault();

		var question = event.target.question.value;
		var choice_one = event.target.choice_one.value;
		var choice_two = event.target.choice_two.value;
		var choice_three = event.target.choice_three.value;
		var choice_four = event.target.choice_four.value;
		var correct_index = event.target.correct_index.value;

		var choicesArray = [choice_one, choice_two, choice_three, choice_four];
		var choicesString = JSON.stringify(choicesArray);

		var dataStore = Backendless.Persistence.of(Trivia);

		var triviaObject = new Trivia({
			question: question,
			choices: choicesString,
			correctAnswer: correct_index
		});

		dataStore.save(triviaObject);
		
		event.target.question.value = "";
		event.target.choice_one.value = "";
		event.target.choice_two.value = "";
		event.target.choice_three.value = "";
		event.target.choice_four.value = "";
		event.target.correct_index.value = "";
	}
});