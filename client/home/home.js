Template.home.helpers({
	yourGames: function () {
		var games = Games.find({
			players: {
				$elemMatch: {
					player: Meteor.userId()
				}
			},
			turn: Meteor.userId(),
			gameStatus: "inProgress"
		});

		return games;
	},
	friends: function () {
		if (Meteor.user().profile.friends) {
			var friends = Meteor.user().profile.friends;
			var friendsList = [];

			for (var i = 0; i < friends.length; i++) {
				friendsList.push(Meteor.users.findOne({
					_id: friends[i]
				}));
			}
			return friendsList;
		}
	},
	opponentData: function () {
		var playerIndex;

		// Checks to see which player is our opponent
		if (this.players[0].player === Meteor.userId()) {
			playerIndex = 1;
		} else if (this.players[1].player === Meteor.userId()) {
			playerIndex = 0;
		}

		return Meteor.users.findOne(this.players[playerIndex].player);
	},
	yourScore: function () {
		var playerIndex;

		// Checks to see which player is our opponent
		if (this.players[0].player === Meteor.userId()) {
			playerIndex = 0;
		} else if (this.players[1].player === Meteor.userId()) {
			playerIndex = 1;
		}

		return this.players[playerIndex].score;
	},
	opponentScore: function () {
		var playerIndex;

		// Checks to see which player is our opponent
		if (this.players[0].player === Meteor.userId()) {
			playerIndex = 1;
		} else if (this.players[1].player === Meteor.userId()) {
			playerIndex = 0;
		}

		return this.players[playerIndex].score;
	},
	profilePic: function (user) {
		var profilePictures;
		var pictureId;

		pictureId = user.profile.picture;
		profilePictures = ProfilePictures.find(pictureId).fetch();

		return profilePictures[0]
	}
});

Template.home.events({
	"click .rand-user": function () {
		var randomUser, canidatePool;

		canidatePool = Meteor.users.find().fetch();
		// removes user from canidatepool
		for (var i = 0; i < canidatePool.length; i++) {
			if (canidatePool[i]._id === Meteor.userId()) {
				canidatePool.splice(i, 1);
				break;
			}
		}

		// Matches user with random player.
		// Extremly inefficient but works for our purposes.
		randomUser = _.sample(canidatePool);

		var game = Games.insert({
			players: [
				{
					player: Meteor.userId(),
					score: 0
				},
				{
					player: randomUser._id,
					score: 0
				}
			],
			turn: Meteor.userId(),
			round: 0,
			gameStatus: "inProgress",
			details: {
				question: ""
			}
		});

		// Calls our game route and passes the games _id as a parameter
		Router.go('game', {
			_id: game
		});
	},
	"click .challenge-friend": function () {
		var game = Games.insert({
			players: [
				{
					player: Meteor.userId(),
					score: 0
				},
				{
					player: this._id,
					score: 0
				}
				],
			turn: Meteor.userId(),
			round: 0,
			gameStatus: "inProgress",
			details: {
				question: ""
			}
		});

		// Calls our game route and passes the games _id as a parameter
		Router.go('game', {
			_id: game
		});
	}
});