function Trivia(args) {
	args = args || {};
	this.question = args.question || "";
	this.choices = args.choices || "";
	this.correctAnswer = args.correctAnswer;
}

Template.game.onRendered(function () {
	var ref = Backendless.Persistence.of(Trivia).find();

	//sets random question
	var randomQuestion = _.sample(ref.data);

	randomQuestion.choices = JSON.parse(randomQuestion.choices);
	Session.set('activeQuestion', randomQuestion);
});

Template.game.helpers({
	opponentIndex: function () {
		return Session.get('opponentIndex');
	},
	currentUserIndex: function () {
		return Session.get('currentUserIndex');
	},
	findScore: function (user) {
		if (typeof this !== 'undefined') {
			return this.players[user].score;
		}
	},
	activeQuestion: function () {
		return Session.get('activeQuestion');
	}
});

Template.game.events({
	"click .pick": function () {
		var question = Session.get('activeQuestion'),
			choiceIndex = question.choices.indexOf(this.valueOf());

		if (choiceIndex === question.correctAnswer) {
			//increments round and score.
			if (Session.get('currentUserIndex') === 0) {
				Games.update(UI.getData()._id, {
					$inc: {
						"players.0.score": 1,
						round: 1
					}
				});
			} else if (Session.get('currentUserIndex') === 1) {
				Games.update(UI.getData()._id, {
					$inc: {
						"players.1.score": 1,
						round: 1
					}
				});
			}

			var ref = Backendless.Persistence.of(Trivia).find();

			//sets random question
			var randomQuestion = _.sample(ref.data);

			randomQuestion.choices = JSON.parse(randomQuestion.choices);
			Session.set('activeQuestion', randomQuestion);
		} else {
			//increments round and changes turn
			if (Session.get('currentUserIndex') === 0) {
				Games.update(UI.getData()._id, {
					$set: {
						turn: UI.getData().players[1].player
					},
					$inc: {
						round: 1
					}
				});
			} else if (Session.get('currentUserIndex') === 1) {
				Games.update(UI.getData()._id, {
					$set: {
						turn: UI.getData().players[0].player
					},
					$inc: {
						round: 1
					}
				});
			}

			Router.go('home');
		}

	}
});