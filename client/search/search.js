Template.search.helpers({
	friended: function () {
		// adds 1 so if index returns zero, spacebars doesnt evaluate it as null
		return Meteor.user().profile.friends.indexOf(this._id) + 1;
	},
	isYou: function () {
		return Meteor.userId() === this._id;
	},
	usersIndex: () => UsersIndex
});

Template.search.events({
	"click .add-friend": function () {
		Meteor.users.update(Meteor.userId(), {
			$addToSet: {
				"profile.friends": this._id
			}
		});
	}
});